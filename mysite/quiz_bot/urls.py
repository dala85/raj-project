
from django.urls import path, include
from quiz_bot import urls
from .views import view_home, question


urlpatterns = [
    path('', view_home, name='view_home'),
    path('question/', question, name='question'),
]
