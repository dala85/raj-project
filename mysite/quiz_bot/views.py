from django.shortcuts import render, redirect

from .score import get_score


questions = {
    'What is the ROC ?',
    # 'What is the AUC ?'
}

answers = {
    'root between false and correct ?',
    # 'AUC  is a u c '
}

score_q = []


def view_home(request):
    global score_q
    return render(request, 'view_home.html', {'score_q': score_q})


def question(request):
    if request.method == 'POST':
        question = request.POST.get('question')

        score = get_score(question)
        print(score)
        global score_q
        score_q.append(score)
        return redirect('view_home')

    else:
        score = None

    context = {'questions': questions, 'answers': answers, 'score': score}
    return render(request, 'question.html', context)
